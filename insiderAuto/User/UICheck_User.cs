﻿using System;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System.Threading;


namespace User
{
    [TestClass]

    public class Users_UI
    {
        private TestContext testContextInstance;
        private IWebDriver driver;
        private string insiderURL;
        private string usersURL;

        //public bool CompareDate()
        //{
        //    string dateFromPage = driver.FindElement(By.CssSelector("_ngcontent-c27")).Text;
        //    DateTime now = DateTime.Now;
        //    DateTime firstDay = new DateTime(now.Year, now.Month, 1);
        //    string firstDayNow = firstDay.ToString("dd MMM yyyy");
        //    string dateSystemNow = DateTime.Now.ToString("dd MMM yyyy");
        //    string dateCompare = firstDayNow + " - " + dateSystemNow;
        //    if (dateFromPage == dateCompare)
        //        ;
        //    else
        //    {
        //        driver.Quit();
        //    }
        //    return true;
        //}

        [TestMethod]
        [TestCategory("Chrome")]
        public void _UsersUI()
        {
            //Login to insider
            driver.Navigate().GoToUrl(insiderURL + "/sad");
            driver.Manage().Window.Maximize();
            driver.FindElement(By.Name("RememberLogin")).Click();
            driver.FindElement(By.Id("Username")).SendKeys("khoa.nguyen");
            driver.FindElement(By.Id("Password")).SendKeys("123456");
            driver.FindElement(By.Name("button")).Click();
            Thread.Sleep(3000);
            driver.Navigate().GoToUrl(usersURL);

            //
            

        }

        //<summary>
        //Gets or sets the test context which provides
        //information about and functionality for the current test run.
        //</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        [TestInitialize()]
        public void SetupTest()
        {

            insiderURL = "http://uat.stsinsider.saigontechnology.vn";
            usersURL = "http://uat.stsinsider.saigontechnology.vn/user";

            //Open Chrome in Incognito mode
            ChromeOptions options = new ChromeOptions();
            options.AddArguments("--incognito");
            driver = new ChromeDriver(@"D:\insiderAuto\insiderAuto\bin\Debug", options);

            //Wait for element appear
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5);

        }

        [TestCleanup()]
        public void MyTestCleanup()
        {
            driver.Quit();
        }
    }
}

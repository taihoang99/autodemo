﻿using System;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System.Threading;

namespace testAuthorization
{
    // <summary>
    // Summary description for MySeleniumTests
    // </summary>
    [TestClass]

    public class BSM_Authorization
    {
        private TestContext testContextInstance;
        private IWebDriver driver;
        private string insiderURL;
        private string projectsListURL;
        private string projectsCreateURL;
        private string timesheetURL;
        private string projectEffortURL;
        private string missedTimesheetURL;
        private string usersURL;

        [TestMethod]
        [TestCategory("Chrome")]
        public void _BSMAuthorization()
        {
            //Login to insider
            driver.Navigate().GoToUrl(insiderURL + "/");
            driver.Manage().Window.Maximize();
            driver.FindElement(By.Name("RememberLogin")).Click();
            driver.FindElement(By.Id("Username")).SendKeys("bsm.test");
            driver.FindElement(By.Id("Password")).SendKeys("P@ssw0rd");
            driver.FindElement(By.Name("button")).Click();

            //Check Sidebar
            driver.FindElement(By.CssSelector(".m-brand__icon.m-brand__toggler.m-brand__toggler--left.m--visible-desktop-inline-block")).Click();
            if (driver.FindElement(By.XPath("//span[contains(@class, 'm-menu__link-text') and text() = ' Dashboard ']")).Displayed)
            {
                ;
            }
            else
            {
                driver.Quit();
            }

            if (driver.FindElement(By.XPath("//span[contains(@class, 'm-menu__link-text') and text() = 'Projects']")).Displayed)
            {
                if (driver.FindElement(By.XPath("//span[contains(@class, 'm-menu__link-text position') and text() = 'Create new project']")).Displayed)
                {
                    ;
                }
                else
                {
                    driver.Quit();
                }
            }
            else
            {
                driver.Quit();
            }

            if (driver.FindElement(By.XPath("//span[contains(@class, 'm-menu__link-text') and text() = ' Timesheet ']")).Displayed)
            {
                ;
            }
            else
            {
                driver.Quit();
            }

            if (driver.FindElement(By.XPath("//span[contains(@class, 'm-menu__link-text') and text() = 'Reports']")).Displayed)
            {
                if (driver.FindElement(By.XPath("//span[contains(@class, 'm-menu__link-text position') and text() = 'Project effort']")).Displayed)
                {
                    ;
                }
                else
                {
                    driver.Quit();
                }

                if (driver.FindElement(By.XPath("//span[contains(@class, 'm-menu__link-text position') and text() = 'Missed timesheets']")).Displayed)
                {
                    ;
                }
                else
                {
                    driver.Quit();
                }
            }
            else
            {
                driver.Quit();
            }

            if (driver.FindElements(By.XPath("//span[contains(@class, 'm-menu__link-text') and text() = ' Users ']")).Count != 0)
            {
                driver.Quit();
            }

            //Check Dashboard Timesheet Calendar 
            if (driver.FindElement(By.CssSelector(".m-portlet__head.calendar-header")).Displayed)
            {
                ;
            }
            else
            {
                driver.Quit();
            }

            //Check Dashboard Total Effort
            if (driver.FindElement(By.CssSelector(".m-portlet__head.total-effort-header")).Displayed)
            {
                ;
            }
            else
            {
                driver.Quit();
            }

            //Check Dashboard OT Timesheet
            if (driver.FindElement(By.CssSelector(".col-md-7.ot-timesheet")).Displayed)
            {
                ;
            }
            else
            {
                driver.Quit();
            }

            //Check Dashboard Invalid Timesheet
            if (driver.FindElement(By.CssSelector(".col-md-5.invalid-timesheet")).Displayed)
            {
                ;
            }
            else
            {
                driver.Quit();
            }

            //Check Projects List
            driver.Navigate().GoToUrl(projectsListURL);
            if (driver.FindElement(By.TagName("list-project")).Displayed)
            {
                //Check Projects Edit
                driver.FindElement(By.CssSelector(".m-datatable__icon.cursor-pointer")).Click();
                if (driver.FindElement(By.TagName("app-edit-project")).Displayed)
                {
                    ;
                }
                else
                {
                    driver.Quit();
                }
            }
            else
            {
                driver.Quit();
            }

            //Check Projects Create
            driver.Navigate().GoToUrl(projectsCreateURL);
            if (driver.FindElement(By.TagName("app-create-new-project")).Displayed)
            {
                ;
            }
            else
            {
                driver.Quit();
            }

            //Check Timesheet
            driver.Navigate().GoToUrl(timesheetURL);
            if (driver.FindElement(By.TagName("app-list-timesheet")).Displayed)
            {
                ;
            }
            else
            {
                driver.Quit();
            }

            //Check Reports Project Effort
            driver.Navigate().GoToUrl(projectEffortURL);
            if (driver.FindElement(By.TagName("project-effort")).Displayed)
            {
                ;
            }
            else
            {
                driver.Quit();
            }

            //Check Reports Missed Timesheets
            driver.Navigate().GoToUrl(missedTimesheetURL);
            if (driver.FindElement(By.TagName("missed-timesheet")).Displayed)
            {
                ;
            }
            else
            {
                driver.Quit();
            }

            //Check Users
            driver.Navigate().GoToUrl(usersURL);
            if (driver.FindElement(By.ClassName("title_not_authorized")).Displayed)
            {
                ;
            }
            else
            {
                driver.Quit();
            }
        }

        // <summary>
        //Gets or sets the test context which provides
        //information about and functionality for the current test run.
        //</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        [TestInitialize()]
        public void SetupTest()
        {

            insiderURL = "http://uat.stsinsider.saigontechnology.vn";
            projectsListURL = "http://uat.stsinsider.saigontechnology.vn/projects/list";
            projectsCreateURL = "http://uat.stsinsider.saigontechnology.vn/projects/create";
            timesheetURL = "http://uat.stsinsider.saigontechnology.vn/timesheet/list-timesheet";
            projectEffortURL = "http://uat.stsinsider.saigontechnology.vn/report/project-effort";
            missedTimesheetURL = "http://uat.stsinsider.saigontechnology.vn/report/missed-timesheet";
            usersURL = "http://uat.stsinsider.saigontechnology.vn/user";

            //Open Chrome in Incognito mode
            ChromeOptions options = new ChromeOptions();
            options.AddArguments("--incognito");
            driver = new ChromeDriver(@"D:\insiderAuto\insiderAuto\bin\Debug", options);

            //Wait for element appear
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5);

        }

        [TestCleanup()]
        public void MyTestCleanup()
        {
            driver.Quit();
        }
    }
}